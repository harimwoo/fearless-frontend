
const payloadCookie = await cookieStore.get('jwt_access_payload')

if (payloadCookie){
    const decoded = atob(payloadCookie.value)
    const payload = JSON.parse(decoded)
    const permission = payload.user.perms
    const newConference = document.querySelector('a[href="new-conference.html"]')
    const newLocation = document.querySelector('a[href="new-location.html"]')
    if(permission.includes("events.add_conference")){
        newConference.classList.remove('d-none')
    }
    if(permission.includes("events.add_location")){
        newLocation.classList.remove('d-none')
    }
}

function createCard(title, subtitle, description, pictureUrl, start, end){
    return `
    <div class="card shadow mb-3">
        <img src="${pictureUrl}" class="card-img-top" alt="...">
        <div class="card-body">
        <h5 class="card-title">${title}</h5>
        <h6 class="card-title mb-2 text-muted">${subtitle}</h5>
        <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
        ${start} ~ ${end}
        </div>
    </div>
  `
}

function createPlaceHolder(){
    return `
    <div class="card" aria-hidden="true">
    <img src="https://placehold.jp/150x150.png" class="card-img-top" alt="...">
    <div class="card-body">
      <h5 class="card-title placeholder-glow">
        <span class="placeholder col-6"></span>
      </h5>
      <p class="card-text placeholder-glow">
        <span class="placeholder col-7"></span>
        <span class="placeholder col-4"></span>
        <span class="placeholder col-4"></span>
        <span class="placeholder col-6"></span>
        <span class="placeholder col-8"></span>
      </p>
      <a href="#" tabindex="-1" class="btn btn-primary disabled placeholder col-6"></a>
    </div>
  </div>
  `
}

function formatDate(dateString){
    const date = new Date(dateString)
    const year = date.getFullYear();
    const month = date.getMonth() + 1
    const day = date.getDate()
    return `${month}/${day}/${year}`
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences'
    try{
        const conferences = await fetch(url)

        if(!conferences.ok){
        } else{
            const data = await conferences.json()
            let num_2 = 0
            for(let i = 0; i < data.conferences.length; i++){
                const column = document.querySelector(`.col:nth-child(${(i%3)+1})`)
                column.innerHTML += createPlaceHolder()
            }
            for(let i = 0; i < data.conferences.length; i++){
                const conference = data.conferences[i]
                const detailUrl = `http://localhost:8000${conference.href}`
                const detailData = await fetch(detailUrl)
                if(detailData.ok){
                    const detail = await detailData.json()
                    const title = detail.conference.name
                    const subtitle = detail.conference.location.name
                    const description = detail.conference.description
                    const pictureUrl = detail.conference.location.picture_url
                    const start = formatDate(detail.conference.starts)
                    const end = formatDate(detail.conference.ends)
                    const html = createCard(title, subtitle, description, pictureUrl, start, end)
                    const column = document.querySelector(`.col:nth-child(${(i%3)+1})`)
                    const hidden = column.querySelector('.card[aria-hidden="true"]')
                    hidden?.remove()
                    column.innerHTML += html

                }
            }
        }
    } catch (e){
        console.error(e)
    }



})

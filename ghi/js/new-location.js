function createStateOption(name, abbreviation){
    return `${abbreviation} - ${name}`
}


window.addEventListener("DOMContentLoaded", async () => {
    const url = "http://localhost:8000/api/states"

    const response = await fetch(url)

    if(response.ok){
        const data = await fetch(url)
        const response = await data.json()

        const stateTag = document.querySelector('#state')

        const states = response.state
        for(let state of states){
            const newState = document.createElement('option')
            newState.innerHTML = createStateOption(state.name, state.abbreviations)
            newState.value = state.abbreviations
            stateTag.appendChild(newState)
        }
    }

    const formTag = document.querySelector('#create-location-form')
    formTag.addEventListener('submit', async event => {
        event.preventDefault()
        const formData = new FormData(formTag)
        const json = JSON.stringify(Object.fromEntries(formData))
        const locationUrl = "http://localhost:8000/api/locations/"
        const fetchConfig = {
            method:"post",
            body:json,
            headers:{
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(locationUrl, fetchConfig)
        if(response.ok){
            formTag.reset()
            const newLocaiton = await response.json()
        }
    })
})

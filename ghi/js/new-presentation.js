window.addEventListener('DOMContentLoaded', async () => {
    //fill the conference drop down
    const url = 'http://localhost:8000/api/conferences'
    const response = await fetch(url)

    if(response.ok){
        const data = await response.json()
        const conferenceTag = document.querySelector('#conference')
        for(let conference of data.conferences){
            const option = document.createElement('option')
            option.value = conference.id
            option.innerHTML = conference.name
            conferenceTag.appendChild(option)
        }
    }
    //post the form data when submitted
    const formTag = document.querySelector('#create-presentation-form')
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        //grab data
        const formData = new FormData(formTag)
        const data = Object.fromEntries(formData)
        console.log(data.conference, typeof data.conference)
        const json = JSON.stringify(Object.fromEntries(formData))
        //set up fetch config
        const presentation_url = `http://localhost:8000/api/conferences/${data.conference}/presentations/`
        const fetchConfig = {
            method:'post',
            body: json,
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(presentation_url, fetchConfig)
        if(response.ok){
            formTag.reset();
            const response_obj = await response.json()
        }
    } )


})

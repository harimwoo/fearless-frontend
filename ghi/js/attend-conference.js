window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences'
    const response = await fetch(url)
    if(response.ok){
        const data = await response.json()
        const selectTag = document.querySelector('#conference')
        const loadingTag = document.querySelector('#loading-conference-spinner')
        for(let conference of data.conferences){
            const option = document.createElement('option')
            option.value = conference.href
            option.innerHTML = conference.name
            selectTag.appendChild(option)
        }
        loadingTag.classList.add('d-none')
        selectTag.classList.remove('d-none')
    }

    const formTag = document.querySelector('#create-attendee-form')
    formTag.addEventListener('submit', async event => {
        event.preventDefault()
        const formData = new FormData(formTag)
        const json = await JSON.stringify(Object.fromEntries(formData))
        const url = 'http://localhost:8001/api/attendees/'
        const fetchConfig = {
            method:'post',
            body: json,
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(url, fetchConfig)
        const successTag = document.querySelector('#success-message')
        if(response.ok){
            formTag.classList.add('d-none')
            successTag.classList.remove('d-none')
        }

    })

})

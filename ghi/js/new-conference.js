window.addEventListener('DOMContentLoaded', async () => {
    const locationUrl = 'http://localhost:8000/api/locations/'

    const response = await fetch(locationUrl)

    if(response.ok){
        const data = await response.json()
        const locationTag = document.querySelector('#location')
        for(let location of data.locations){
            const option = document.createElement('option')
            option.value = location.id
            option.innerHTML = location.name
            locationTag.appendChild(option)
        }
    }
    const formTag = document.querySelector('#create-conference-form')
    formTag.addEventListener('submit', async event =>{
        event.preventDefault()
        const formData = new FormData(formTag)
        const json = JSON.stringify(Object.fromEntries(formData))
        const conferenceUrl = 'http://localhost:8000/api/conferences/'
        const fetchConfig = {
            method: 'post',
            body: json,
            header: {
                "Content-Type": 'application/json'
            }
        }
        console.log(json)
        const response = await fetch(conferenceUrl, fetchConfig)
        if(response.ok){
            formTag.reset()
            const newConference = await response.json()
        }
    })


})
